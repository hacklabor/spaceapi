<?php

define('MESSAGE_OPEN', "Yes we're open!");
define('MESSAGE_CLOSED', "Sorry we're closed!");

parse_str($_SERVER['QUERY_STRING'], $result);

if (!isset($result['request'])) {
  $endpoint = 'default';
} else {
  $args = explode('/', rtrim($result['request'], '/'));
  $endpoint = $args[0];
}

$method = $_SERVER['REQUEST_METHOD'];
$data = [];

if ($method == 'POST' || $method == 'PUT') {
    // Write-Access from Hacklabor only
    /*
    if ($_SERVER['REMOTE_ADDR'] != '2001:470:75f7:2342:6063:35ff:fe34:3031') {
        header('HTTP/1.0 403 Forbidden');
	    print_spaceapijson();
    }
    */

	switch ($_SERVER["CONTENT_TYPE"]) {
		case 'application/json':
			$data = json_decode(file_get_contents("php://input"), true);
			break;

		case 'application/x-www-form-urlencoded':
		case 'multipart/form-data':
		default:
			parse_str(file_get_contents("php://input"),$data);
			break;
	}

	array_walk_recursive($data, 'set_boolean');
}

switch ($endpoint) {
	case 'state':	
		switch ($method) {
			case 'POST':
			case 'PUT':
				set_state($data);
				break;
			
			default:
				
				break;
		}
		break;
	
	default:
		
		break;
}

print_spaceapijson();

function set_state($data) {
	// Limit to state->open and state->message
	$set_vars = ['open' => 'bool', 'message' => 'string'];

	$spaceapijson = read_spaceapijson();
	
	foreach ($set_vars as $key=>$type) {
		if(isset($data[$key])) {
			$spaceapijson['state'][$key] = $data[$key];	
		}				
	}

	if (empty($data['message'])) {
		switch ($spaceapijson['state']['open']) {
			case true:
				$spaceapijson['state']['message'] = MESSAGE_OPEN;
				break;
			
			default:
				$spaceapijson['state']['message'] = MESSAGE_CLOSED;
				break;
		}
	}
	
	$spaceapijson['state']['lastchange'] = time();
	write_spaceapijson($spaceapijson);
}

function read_spaceapijson() {
	$spaceapijson = json_decode(file_get_contents('spaceapi.tpl.json'), true);
	return $spaceapijson;
}

function write_spaceapijson($spaceapijson) {
	file_put_contents('spaceapi.json', json_encode($spaceapijson));
}

function print_spaceapijson() {
	header('Content-Type: application/json');
	if ($spaceapijson = file_get_contents('spaceapi.json') ) {
		echo $spaceapijson;
	} else {
		echo json_encode([]);
	}
	exit();
}

function clean_string($string) {
	$string = strip_tags((string)$string);
	$string = trim($string);
	return $string;
}

function set_boolean(&$item, $key) {
	if (!is_string($item)) $item = (bool) $item;

	switch (strtolower($item)) {
	    case '1':
	    case 'true':
	      $item = true;
	      break;
	    default:
	      $item = false;
	}
}
