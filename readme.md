** Hints **

  * Setting state works from Hacklabor IP only.  
  * Changing paths requires changes in .htaccess

**Show Space Info**
----
  Returns json data about the space.

* **URL**

  /api/space/v1/

* **Method:**

  `GET`
  
*  **URL Params**

  None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ ... }`
 
* **Error Response:**

  * **Code:** 404 <br />
    **Content:** `<html>....`

  OR

  * **Code:** 500 <br />
    **Content:** None

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/space/v1/",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```

  **Set Space State**
----
  Returns json data about the space.

* **URL**

  /api/space/v1/state

* **Method:**

  `PUT`
  
*  **URL Params**

  None

* **Data Params**

  **Required:**
 
   `open=[boolean]`

  **Optional:**
 
   `message=[string]`

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ ... }`
 
* **Error Response:**

  * **Code:** 404 <br />
    **Content:** `<html>....`

  OR

  * **Code:** 500 <br />
    **Content:** None

* **Sample Call:**

  ```javascript
    var dataObject = { 'open': true, 'message': "Yes we're open" };

    $.ajax({
      url: "/api/space/v1/state",
      dataType: "json",
      data: JSON.stringify(dataObject),
      type : "PUT",
      success : function(r) {
        console.log(r);
      }
    });
  ```